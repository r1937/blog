---
title: "Connect your Rapsberry Pi to WiFi, SSH, and VNC"
date: 2020-11-03T17:11:33+01:00
description: "Connect your rapsberry to wifi, enable SSH and VNC"
tags: ["Raspberry Pi", "wifi", "ssh", "vnc"]
categories: ["Electronics"]
series: ["Getting Started with Raspberry Pi"]
draft: false  
---

This is the first part of starting interacting with your Raspberry. The options described in this post will allow you to connect your RPi to WiFi and access it through SSH or VNC.

<!--more-->

We can enable certain features on boot by creating config files in the boot Micro SD partition. 

We have two categories of options:
1. Headless: Modify the boot files of the OS in the Micro SD from your laptop
2. GUI: Connect peripherals, i.e. monitor, keyboard and mouse.

We will focus on the Headless configuration. 

#### Pre Requisite
- Raspbian has been installed on a micro sd 

### Enable Wifi
Create a file in the boot folder called `wpa_supplicant.conf`. Use the following template and substitute your ISO country code, network name and password:

```bash
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
country=<Insert 2 letter ISO 3166-1 country code here>

network={
        scan_ssid=1
        ssid="<Name of your wireless LAN>"
        psk="<Password for your wireless LAN>"
        proto=RSN
        key_mgmt=WPA-PSK
        pairwise=CCMP
        auth_alg=OPEN
}
```

Make sure to edit the file with a cli tool or IDE that won't add any weird end of line behaviour, i.e. no rich text editors. 

### Enable SSH
Create a file called `ssh` in the boot directory. It doesn't require an extension. Once your rpi boots it will enable it if it finds this file, and it will also remove the file.

**Note:** If SSH is failing to activate have a look at your Micro SD card again and confirm that you are adding the ssh file into the correct boot partition. 

### Retrieve RPi IP
This is a bit tricky as we would need access to the RPi to find either the IP or the MAC address but we have a few options:
1. Find your RPi through the ARP table in your router based on the MAC address 
2. Connect to your computer with an ethernet patch cord and ping `raspberrypi.local
3. If all else fails, connect peripherals and access your RPI through a monitor and keyboard

Option 1 is my prefered option but it may be cumbersome if you have too many RPis already in your network. That being said you can filter out your options based on the version of your rpi.

RPis have common Organization Unique Identifiers (OUIs). Take a look at this [IEEE OUI list](http://standards-oui.ieee.org/oui/oui.txt)  and search for Raspberry. You'll notice that there are three entries which start with `B8-27-EB`, `DC-A6-32` , and `E4-5F-01` . You can now note down the IPs for the devices with MAC addresses starting with these OUIs. Quick tip, version 4 RPis will be using `E4-5F-01`. 

After finding the IP, connect to your RPi using a terminal in your Linux or Mac machine or SSHPuty in Windows. For CLI use `ssh pi@<YOUR-IP>`. 

The default username is `pi` and the default password is `raspberry`. Make sure to change this after your first log in.

### Enable VNC

While connected through SSH you'll need to install the VNC server.

```
sudo apt update
sudo apt install realvnc-vnc-server realvnc-vnc-viewer
```

Now that it's installed let's enable it by calling `sudo raspi-config`. Find the `Interface Options`, `VNC` and enable. 

### Connect to RPi through VNC

I use [RealVNC](https://www.realvnc.com/en/) but feel free to choose your prefered VNC client. Download the client application to your machine and create a new connection. The host or server will be your RPi's IP address. Optionally, but preferably, add a name to it to make it easier to identify in the future. Save the connection.

The first time you connect it will ask for confirmation, then it asks for the credentials that we modified in the `Retrieve RPi IP` section. 

### Final Thoughts
You can now access your RPi remotely which gives many more project opportunities. While SSH access is great we will also need a graphic interface for some features. In the next step in this series, we will set up VNC access to connect remotely to our Raspberry Pi Desktop environment.

### Reference
- “Raspberry Pi Documentation.” Configuration, https://www.raspberrypi.com/documentation/computers/configuration.html#setting-up-a-headless-raspberry-pi.
- “Raspberry Pi Documentation.” Remote Access, https://www.raspberrypi.com/documentation/computers/remote-access.html#ssh.
