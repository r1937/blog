---
title: "Prepare your Raspberry Pi Micro SD"
date: 2020-11-03T17:11:33+01:00
description: "Setup access to your Raspberry Pi through VNC"
tags: ["Raspberry Pi", "VNC"]
categories: ["Electronics"]
series: ["Getting Started with Raspberry Pi"]
draft: false  
---

The first step in working with your Raspberry Pi is to set up your Micro SD. Not a complex process, but it's good to have a straightforward approach to follow.

<!--more-->

### Pre requisites
- Micro SD with a minimum of 8GB
- Micro SD card reader
- Flashing software. 
	- [Raspberry Pi Imager](https://www.raspberrypi.com/software/), recommended, or
	- [Etcher](https://www.balena.io/etcher/) if you want to manually add the image

### Prepare Micro SD
1. Download the Raspberri Pi Imager software from the link above
2. The installation formats the card for you, so you only need to choose the OS, which will most likely be Raspberry Pi OS. 
3. Choose the Micro SD as the storage and start the process

### Next Steps
Don't remove the Micro SD card just yet. We will be configuring network access in the [next step](../raspberry-wifi-ssh).