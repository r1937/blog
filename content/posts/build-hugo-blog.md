---
title: "Create your blog with Hugo (1/3)"
date: 2022-01-08T14:17:33+01:00
description: "Build a SSG blog with Hugo."
tags: ["blog", "Hugo"]
categories: ["Software"]
series: ["Create and deploy you Blog with Hugo, GitLab and AWS"]
draft: false
---

This is the first post of the `Create and deploy you Blog with Hugo, GitLab and AWS` series.

In this post, we will focus on setting up Hugo along with a theme to get you started blogging faster.

<!--more-->

### Tech Stack
- Hugo for our SSG
- GitLab to host our code 
- GitLab CI/CD for packaging and releasing
- AWS S3 to host our static site
- Cloudfront as our CDN

## Install
Homebrew is a typical installation option for Hugo in both Mac and Linux. 

```bash
brew install hugo
hugo version
```

After the second command above you should get your Hugo version.

If you're using Windows take a look at Chocolatey as a package manager to install Hugo.

## Local Development
Create a site using hugo cli:

```bash
> hugo new site <SITE-NAME>

Congratulations! Your new Hugo site is created in /Users/b4h72f/<SITE-NAME>.

Just a few more steps and youre ready to go:

1. Download a theme into the same-named folder.
   Choose a theme from https://themes.gohugo.io/ or
   create your own with the "hugo new theme <THEMENAME>" command.
2. Perhaps you want to add some content. You can add single files
   with "hugo new <SECTIONNAME>/<FILENAME>.<FORMAT>".
3. Start the built-in live server via "hugo server".

Visit https://gohugo.io/ for quickstart guide and full documentation.
```

Now let's add a theme by downloading one and including it in the themes directory.

```bash
cd <SITE-NAME>
git init
git submodule add https://github.com/adityatelange/hugo-PaperMod.git themes/papermodl2
```

Add the theme to the site configuration:

```bash
echo theme = \"papermodl2\" >> config.toml
```

Finally, start the server with `hugo serve` and visit the first iteration of your blog at http://localhost:1313 

## Usage

### Config
You probably noticed that the information such as the title uses some default values. Let's take care of that first.

Open the `config.toml` file and change the information you need to be updated, preferably with your prefered IDE. Try changing the title; Hugo is listening for file changes and automatically refresh the page. 

Do note that the theme you use will have its features, so don't forget to visit the README and Wiki for your theme. The one that we're using here can be found in [this repo](https://github.com/adityatelange/hugo-PaperMod) and [this Wiki](https://github.com/adityatelange/hugo-PaperMod/wiki/Installation#sample-configyml) describes the config options.

I prefer to use YAML over TOML so I keep my config file in YAML format but just know that you can switch between them and as long as you reformat the file it should work as expected. 

### Content

To add your first post type `hugo new posts<POST-NAME>` in your terminal. This will create a file under `<PROJECT-ROOT>/content/posts/` with a metadata block. You are now able to extend the metadata and add you content below the block.

```markdown
---
title: "My First Post"
date: 2021-01-09T15:23:47+01:00
draft: true
---

Add your content here
```

After you add some content to this document, change the `draft` attribute on the metadata block from `true` to `false`, and you should be able to see it on your browser.


### Analytics and Other Features

Most themes have a way to integrate with Google Analytics or other platforms such as sharing through social media, search as a service platform and more. You'll find more in the theme's documentation; let's integrate this blog with Google Analytics as a quick example.

You will need a Google Analytics account. Once you have it, you can navigate to the Admin panel to create a new Property. You can name it something like `Blog` and once it's created, select the Property Settings option. You'll find the `Tracking Id` on this screen, which has a format like `UA-12345678-1`. Copy the tracking Id and go back to your config file. In this file add the attribute `googleAnalytics: UA-42375834-1` or in the TOML format `googleAnalytics = "UA-42375834-1"`, save and it's ready for release. 

### Build

Call `hugo -D`. You'll find the site content in the public directory of your project. Don't worry too much about what to do with it just yet. For now, let's make sure we keep a clean repo by adding a `.gitignore` file in the root of your project and a `.gitkeep` file in the public directory. 

Now, open the `.gitignore` file and include the following content:

```
public/*
!public/.gitkeep
```
This will avoid duplicating the content in our repo and keep our development content separate from our production content while allowing us to run builds locally for testing purposes. 

## Save Your Changes
You should already have initiated the root directory as a git repo after downloading the theme. Make sure to create a project in your remote git server. In our next post, we will be using GitLab, so create an empty project there and add the project's URL as the origin to your project. Add, commit and push your changes to GitLab.

## Final Thoughts
We have built a blog with Hugo and configured it with a theme. With this in place, we can add content and tweak our theme, but it's only accessible locally. You can manually upload the content of your public directory after you build the site, but I much prefer to automate it after pushing my changes to a remote git server.

The next step is to set up a CICD pipeline with GitLab and an S3 bucket to host our site. Follow this [link](../deploy-hugo-blog/) to next step in our series.

## Reference
- “Get Started.” Hugo, https://gohugo.io/getting-started.