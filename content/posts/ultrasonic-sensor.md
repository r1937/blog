---
title: "Configure an Ultrasonic Sensor with a Raspberry Pi"
date: 2021-09-17T17:11:33+01:00
description: "Configure an ultrasonic HC-SR04 with a Raspberry Pi"
tags: ["Raspberry Pi", "sensors", "ultrasonic"]
categories: ["Electronics"]
draft: true
-----

